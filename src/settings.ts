import '@logseq/libs'
import { SettingSchemaDesc } from '@logseq/libs/dist/LSPlugin.user';

export const settingUI = () => {
    /* https://logseq.github.io/plugins/types/SettingSchemaDesc.html */
    const settingsTemplate: SettingSchemaDesc[] = [
        {
            key: "APIKey",
            type: "string",
            title: "API Key",
            description: "Reclaim.ai API Key",
            default: null,
            inputAs: "textarea"
        }
    ];
    logseq.useSettingsSchema(settingsTemplate);
};
