import '@logseq/libs'
import { BlockEntity, IDatom, IHookEvent } from '@logseq/libs/dist/LSPlugin';
import moment, { Moment } from 'moment';
import { settingUI } from './settings';

const url = 'https://api.app.reclaim.ai/api';

async function observable(e: IHookEvent & { blocks: BlockEntity[]; txData: IDatom[]; txMeta?: { [key: string]: any; outlinerOp: string; } | undefined; }): Promise<void> {
    for (let block of e.blocks) {
        if (block.content && block.content.startsWith("TODO ")) {
            if (!block.properties || !("reclaim.id" in block.properties))
                await todo(block.uuid);
        }
        else if (block.content && block.content.startsWith("DONE ")) {
            if (block.properties && "reclaim.id" in block.properties)
                await done(block.uuid);
        }
    }
}

async function todo(uuid: string) {
    const block = await logseq.Editor.getBlock(uuid);
    if (!block) return;

    const block_url = await logseq.Editor.getBlockProperty(uuid, "url");
    const block_time = await logseq.Editor.getBlockProperty(uuid, "time");
    const snooze = block.scheduled ? moment(block.scheduled, "YYYYMMDD") : null;
    const due = block.deadline ? moment(block.deadline, "YYYYMMDD") : null;

    const body: Partial<{ title: string, [key: string]: any, snoozeUntil: Moment | null, due: Moment | null }> = {
        title: block.content.split('\n')[0].trim().replace("TODO ", ''),
        eventColor: null,
        eventCategory: 'WORK',
        timeChunksRequired: block_time ? parseInt(block_time) * 4 : 4,
        minChunkSize: 4,
        maxChunkSize: 16,
        priority: 'DEFAULT',
        alwaysPrivate: false,
        snoozeUntil: snooze,
        due: due,
        notes: block_url ? block_url : null
    };

    const apikey = logseq.settings ? logseq.settings['APIKey'] : null;
    await fetch(url + "/tasks", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${apikey}`
        },
        body: JSON.stringify(body)
    }).then(async res => {
        const resJson = await res.json();
        await logseq.Editor.exitEditingMode();
        await logseq.Editor.upsertBlockProperty(block.uuid, 'reclaim.id', resJson.id);
    })
}

async function done(uuid: string) {
    const block = await logseq.Editor.getBlock(uuid);
    if (!block) return;

    const reclaimId = await logseq.Editor.getBlockProperty(block.uuid, 'reclaim.id');
    if (!reclaimId) return;

    const apikey = logseq.settings ? logseq.settings['APIKey'] : null;
    await fetch(url + `/planner/done/task/${reclaimId}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${apikey}`
        },
        body: JSON.stringify({})
    }).then(async res => {
        await logseq.Editor.removeBlockProperty(block.uuid, 'reclaim.id');
    })
}

function main() {

    settingUI();

    logseq.DB.onChanged(observable);
}

logseq.ready(main).catch(console.error)
