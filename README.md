# Logseq plugin for Reclaim.ai

Schedules Logseq TODOs in Reclaim.ai and marks them as done when marked done in Logseq.


## Configuration
Just set the API Key in the Settings.

## How to use it.

* Items marked as `TODO` create a Reclaim.ai task. The first line of the block is the title.
  * Configurable properties:
    * `url::`: Sets the notes
    * `time::` (e.g. "3h"): Sets the length to block for
    * `SCHEDULED:` Sets the snooze
    * `DEADLINE:` Sets the deadline
  * `reclaim.id::` property gets set with the Reclaim.ai Id
* Items marked as `DONE` get marked done in Reclaim.ai. The `reclaim.id::` property gets removed

NOTE: As soon as a block starts with `TODO ` it gets sent to Reclaim.ai. So it's a good idea to write the block first, then mark it as a TODO.

## How to obtain an API Key
While inspecting the network calls on the Reclaim.ai website, create a dummy task then look for the cookies sent with the request. The API Key will be in there.
